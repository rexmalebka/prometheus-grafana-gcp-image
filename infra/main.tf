provider "google" {
  project = "dou-hashicorp-lab"
  region  = "us-central1"
  zone    = "us-central1-c"
}

data "google_compute_image" "grafana-prometheus_image" {
  name    = "grafana-prometheus"
}

resource "google_compute_instance" "default" {
  name        = "prometheus-grafana"

  zone    = "us-central1-c"
  tags = ["grafana-prometheus","telemetry","consul","vault"]

  labels = {
    environment = "dev"
  }

  machine_type         = "e2-micro"
  can_ip_forward       = true

  scheduling {
    automatic_restart   = true
    on_host_maintenance = "MIGRATE"
  }

  // Create a new boot disk from an image
  boot_disk {
    initialize_params {
	    image = data.google_compute_image.grafana-prometheus_image.self_link
	}
  }

  network_interface {
    network = "default"
    access_config {
      // Include this section to give the VM an external ip address
    }
  }


  service_account {
    scopes = ["userinfo-email", "compute-ro", "storage-ro"]
  }
}

resource "google_compute_firewall" "http-server" {
  name    = "grafana-prometheus"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["9090","3000"]  
  }

  // Allow traffic from everywhere to instances with an http-server tag
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["grafana-prometheus"]
}
