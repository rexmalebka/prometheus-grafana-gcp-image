# prometheus + grafana GCP Image for consul service discovery

## how to

1.- build Image

```bash
cd packer

GOOGLE_APPLICATION_CREDENTIALS=creds.json PROJECT_ID=project-id ./build-image.sh
```

2.- create instance with terraform

```
terraform init
GOOGLE_APPLICATION_CREDENTIALS=creds.json PROJECT_ID=project-id terraform plan
GOOGLE_APPLICATION_CREDENTIALS=creds.json PROJECT_ID=project-id terraform apply
``` 


## ansible roles variables

- prometheus_url: for specifying prometheus tar.gz download
- prometheus_directory: for specifying prometheus directory
- prometheus_consul_sd: list of consul nodes where to scrape
- prometheus_bearer_token: vault token with permissions to read metrics

- grafana_url: grafana url where the deb is

